/*
Scott D. Allen  / Abigail Allen
*/

#include <conio.h>
#include <iostream>
#include <string>

using namespace std;


// Declare enumerations containing the value of the suit
// If this game of cards was played with suit ranking,
// the values could be assigned by alphabetical order, 
// or another system, like alternating colors
enum Suit
{
	HEARTS,
	DIAMONDS,
	CLUBS,
	SPADES
};

// Declare enumerations containing the value of the rank
// This is what determines the return value of the HighCard function
enum Rank
{
	// Setting the initial value to 2, so the rest of the enumerations
	// continue after the weakest rank, up to a high of 14 (ace)
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

// Declares the Card struct
struct Card
{
	Suit suit;
	Rank rank;

};

// Prototyping
void PrintCard(Card card);

Card HighCard(Card card1, Card card2);


int main()
{
	// Declaring two different instances of the Card
	// data type, each with different ranks and suits
	Card Ace_Of_Spades;
	Ace_Of_Spades.rank = ACE; // <-- 14
	Ace_Of_Spades.suit = SPADES;

	Card Two_Of_Diamonds;
	Two_Of_Diamonds.rank = TWO; // <-- 2
	Two_Of_Diamonds.suit = DIAMONDS;
	
	PrintCard(Two_Of_Diamonds);
	_getch();
	
}

void PrintCard(Card card)
{
	// I think I was also confused by this section the first time I took the course.
	// Is there a better way to print the cards by name using the enumerations?
	// I considered creating arrays to match the enum values to different strings,
	// but I figured I was making things harder than they needed to be. 

	if (card.rank == 2 && card.suit == DIAMONDS)
	{
		cout << "The Two of Diamonds";
	}
};


Card HighCard(Card card1, Card card2)
{
	// Takes two arguments of the Card data type, and returns
	// the one with the greater rank.
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else if (card1.rank < card2.rank)
	{
		return card2;
	}
};